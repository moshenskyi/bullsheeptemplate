package com.moshenskyi.sceneformtemplate;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory {
    // TODO: 07.05.19 Enter endpoint
    private static final String API_ENDPOINT = "";
    private static volatile MyService myService;

    @NonNull
    public static MyService getMyService() {
        if (myService == null) {
            synchronized (ApiFactory.class) {
                if (myService == null) {
                    myService = new Retrofit.Builder()
                            .baseUrl(API_ENDPOINT)
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .client(buildOkHttpClient())
                            .build()
                            .create(MyService.class);
                }
            }
        }
        return myService;
    }

    private static OkHttpClient buildOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(12, TimeUnit.SECONDS)
                .readTimeout(12, TimeUnit.SECONDS)
                .addInterceptor(createLogInterceptor())
                .build();
    }

    private static HttpLoggingInterceptor createLogInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

}
